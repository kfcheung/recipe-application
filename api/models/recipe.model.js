module.exports = mongoose => {
    var schema = mongoose.Schema(
      {
        account:String,
        title: String,
        energy: String,
        fat_label: String,
        fat_qty: String,
        fat_unit: String,
        carbs_label:String,
        carbs_qty:String,
        carbs_unit:String,
        protein_label:String,
        protein_qty:String,
        protein_unit:String
        
      }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    const Recipe = mongoose.model("recipe", schema);
    return Recipe;
  };